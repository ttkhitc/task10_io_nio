import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship<Droid> implements Serializable {

	private List<Droid> ship;

	public Ship() {

		ship = new ArrayList();
	}

	public void setToShip(Droid d) {
		ship.add(d);
	}

	public List<Droid> getShip() {

		return ship;
	}

	@Override
	public String toString() {
		return "Ship [ship=" + ship + ", getShip()=" + getShip() + "]";
	}
	
	

}
