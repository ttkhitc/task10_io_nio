import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.omg.CORBA.OBJECT_NOT_EXIST;

public class Main {

	private static void serialize(Ship<Droid> s) {

		try (ObjectOutputStream serialize = new ObjectOutputStream(
				new FileOutputStream("C:\\Users\\Taras\\eclipse-workspace\\HWIo_NIO\\Droids\\ForSerialize.txt"))) {
			serialize.writeObject(s);
		} catch (Exception ex) {

			System.out.println(ex.getMessage());
		}
	}

	private static Object deSerialize() {

		try (ObjectInputStream deserialize = new ObjectInputStream(
				new FileInputStream("C:\\Users\\Taras\\eclipse-workspace\\HWIo_NIO\\Droids\\ForSerialize.txt"))) {

			return deserialize.readObject();
		} catch (Exception ex) {

			System.out.println(ex.getMessage());
		}

		return new OBJECT_NOT_EXIST();
	}

	public static void main(String[] args) {

		Ship<Droid> myShip = new Ship<Droid>();

		myShip.setToShip(new Droid("AM-18", "Valera", 100, 01));
		myShip.setToShip(new Droid("Ak-47", "Matras", 150, 02));
		myShip.setToShip(new Droid("Barret", "Killer", 500, 03));
		myShip.setToShip(new Droid("Ak-47", "Dumy", 150, 04));
		myShip.setToShip(new Droid("M-16", "LastOne", 130, 05));

		serialize(myShip);
		System.out.println("Serializing.....Success!\n");
		System.out.println(deSerialize());
		System.out.println("\nDeserializing.....Success!\n");
	}
}
