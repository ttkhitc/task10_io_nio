import java.io.Serializable;

public class Droid implements Serializable {

	private String weapon;

	private String DronName;

	private int damage;

	private transient int serialNumber;

	public Droid(String weapon, String dronName, int damage, int serialNumber) {

		this.weapon = weapon;
		DronName = dronName;
		this.damage = damage;
		this.serialNumber = serialNumber;
	}

	public Droid() {

	}

	public String getWeapon() {
		return weapon;
	}

	public void setWeapon(String weapon) {
		this.weapon = weapon;
	}

	public String getDronName() {
		return DronName;
	}

	public void setDronName(String dronName) {
		DronName = dronName;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
	public String toString() {
		return "Droid [getWeapon()=" + getWeapon() + ", getDronName()=" + getDronName() + ", getDamage()=" + getDamage()
				+ ", getSerialNumber()=" + getSerialNumber() + "]";
	}

	
}
