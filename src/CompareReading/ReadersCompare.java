import java.io.*;

public class ReadersCompare {

	private final static String file = "C:\\Users\\Taras\\eclipse-workspace\\HWIo_NIO\\CompareReading\\����� �. - Java 8. ������ �����������. 9-� ������� (2015).pdf";

	
	public static void bufferedReader() {
		
		try {
			long startTime = System.currentTimeMillis();
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			String line = null;
			
			StringBuilder stringBuilder = new StringBuilder();

			
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			
			long timeSpent = System.currentTimeMillis() - startTime;
			
			System.out.println("Time for reading file with BufferedReader = " + timeSpent + " MS");
			
			reader.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} 
	}
	
public static void bufferedReaderWith_10MB() {
		
		try {
			long startTime = System.currentTimeMillis();
			
			BufferedReader reader = new BufferedReader(new FileReader(file), 10240);
			
			String line = null;
			
			StringBuilder stringBuilder = new StringBuilder();

			
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			
			long timeSpent = System.currentTimeMillis() - startTime;
			
			System.out.println("Time for reading file with BufferedReader of 10MB = " + timeSpent + " MS");
			
			reader.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} 
	}

public static void bufferedReaderWith_1MB() {
	
	try {
		long startTime = System.currentTimeMillis();
		
		BufferedReader reader = new BufferedReader(new FileReader(file), 1024);
		
		String line = null;
		
		StringBuilder stringBuilder = new StringBuilder();

		
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
		}
		
		long timeSpent = System.currentTimeMillis() - startTime;
		
		System.out.println("Time for reading file with BufferedReader of 1MB = " + timeSpent + " MS");
		
		reader.close();
		
	} catch (IOException e) {
		
		e.printStackTrace();
		
	} 
}
	

	public static void fileReader() {
		try {
			long startTime = System.currentTimeMillis();

			FileReader reader = new FileReader(file);

			StringBuilder stringBuilder = new StringBuilder();

			int data = reader.read();

			while (data != -1) {

				stringBuilder.append(((char) data));

				data = reader.read();
			}

			long timeSpent = System.currentTimeMillis() - startTime;

			System.out.println("Time for reading file with fileReader = " + timeSpent + " MS");

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static void inputStreamReader () {
		
		try {
			long startTime = System.currentTimeMillis();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			
			String line = null;
			
			StringBuilder stringBuilder = new StringBuilder();

			
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
			}
			
			long timeSpent = System.currentTimeMillis() - startTime;
			
			System.out.println("Time for reading file with InputStream reader = " + timeSpent + " MS");
			
			reader.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} 
	}
	
	public static void go() {
		
		bufferedReader();
		fileReader();
		bufferedReaderWith_10MB();
		bufferedReaderWith_1MB();
		inputStreamReader();
	}

	public static void main(String[] args) {

		go();

	}
}
